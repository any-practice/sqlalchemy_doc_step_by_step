from sqlalchemy.orm import Session

from src.core.engine import engine
from src.orm.declarative_models.case2 import DepartamentORM, ProgrammerORM
from src.orm.declarative_models.case2.base import Base


def create_tables():
    Base.metadata.create_all(engine)


def create_entrys():
    with Session(engine) as session:
        sql_programmers = DepartamentORM(name='sql_programmer')
        infrastructure = DepartamentORM(name='infrastructure')
        backend = DepartamentORM(name='backend')

        sql_programmer1 = ProgrammerORM(
            age=10,
            salary=2500,
            departament=sql_programmers,
        )
        sql_programmer2 = ProgrammerORM(
            age=12,
            salary=2600,
            departament=sql_programmers,
        )
        sql_programmer3 = ProgrammerORM(
            age=19,
            salary=500,
            departament=sql_programmers,
        )
        infrastructure_programmer1 = ProgrammerORM(
            age=18,
            salary=500,
            departament=infrastructure,
        )
        infrastructure_programmer2 = ProgrammerORM(
            age=21,
            salary=750,
            departament=infrastructure,
        )
        infrastructure_programmer3 = ProgrammerORM(
            age=35,
            salary=1800,
            departament=infrastructure,
        )
        backend_programmer1 = ProgrammerORM(
            age=18,
            salary=5000,
            departament=backend,
        )
        backend_programmer2 = ProgrammerORM(
            age=21,
            salary=4000,
            departament=backend,
        )
        backend_programmer3 = ProgrammerORM(
            age=35,
            salary=3000,
            departament=backend,
        )
        session.add_all(
            (
                sql_programmers,
                infrastructure,
                backend,
                sql_programmer1,
                sql_programmer2,
                sql_programmer3,
                infrastructure_programmer1,
                infrastructure_programmer2,
                infrastructure_programmer3,
                backend_programmer1,
                backend_programmer2,
                backend_programmer3,
            ),
        )
        session.commit()


def fetch_diff_salary_with_avg_salary_in_departament():
    """
    получаем разницу зарплаты программиста с средней зарплатой с департаменте, в котором он находится
    """
    '''
    sql запрос

    with helper2 as (
        select *, p_salary - avg_salary_in_departament as salary_diff
        from (
                select d.id                                        as d_id,
                        p.id                                        as p_id,
                        p.salary                                    as p_salary,
                        avg(p.salary) over (partition by d.id)::int as avg_salary_in_departament
                from programmer p
                        inner join departament d on p.departament_id = d.id
            ) as helper1
    )
    select * from helper2
    order by salary_diff desc
    ;
    '''

    from sqlalchemy import select, Integer
    from sqlalchemy.sql import func
    from sqlalchemy.orm import aliased

    d = aliased(DepartamentORM)
    p = aliased(ProgrammerORM)

    with Session(engine) as session:
        # yapf: disable
        subquery = select(
            d.id.label('d_id'),
            p.id.label('p_id'),
            p.salary,
            func.avg(p.salary).over(partition_by=d.id).cast(Integer).label('avg_salary_in_departament'),
        ).select_from(
            p,
        ).join(
            d,
            onclause=p.departament_id == d.id,  # это может опустить, просто для практики написал
        ).subquery('helper_1')
        # yapf: enable

        # yapf: disable
        cte = select(
            subquery.c.d_id,
            subquery.c.p_id,
            subquery.c.salary,
            (subquery.c.salary - subquery.c.avg_salary_in_departament).label('salary_diff'),
        ).cte(
            'helper_2',
        )
        # yapf: enable

        # yapf: disable
        query = select(
            cte.c.d_id,
            cte.c.p_id,
            cte.c.salary,
            cte.c.salary_diff,
        ).select_from(
            cte,
        ).order_by(
            cte.c.salary_diff.desc(),
        )
        # yapf: enable

        result = session.execute(query)

        for row in result:
            row = row._asdict()
            departament_id = row["d_id"]
            person_id = row["p_id"]
            person_salary = row["salary"]
            salary_diff = row["salary_diff"]
            print(
                departament_id,
                person_id,
                person_salary,
                salary_diff,
            )
            print()


def fetch_departaments_with_count_of_programmers_on_it():
    """
    хочу сфетчить департаменты, чтобы при этом у них был атрибут, равный значению
    количества программистов в этом департаменте

    # по сути это annotate джанговый
    """
    '''
    -- получаем все департаменты с количеством программистов в каждом из них
    select
        d.*,
        count(p.id) over (partition by d.id) as count_of_programmers
    from departament d
    left join programmer p on d.id = p.departament_id
    ;
    '''

    from sqlalchemy import select
    from sqlalchemy.sql import func
    from sqlalchemy.orm import aliased

    p = aliased(ProgrammerORM)
    d = aliased(DepartamentORM)

    with Session(engine) as session:
        # yapf: disable
        query = select(
            d,
            func.sum(p.id).over(partition_by=d.id).label('count_of_programmers'),
        ).select_from(
            d,
        ).join(
            p,
            isouter=True,  # left join
        )
        # yapf: enable

        result = session.execute(query)

        for departament, count_of_programmers in result:
            print(departament)    # DepartamentOrm object
            print(count_of_programmers)    # int
            print()


# main = create_tables
# main = create_entrys
# main = fetch_diff_salary_with_avg_salary_in_departament
main = fetch_departaments_with_count_of_programmers_on_it

if __name__ == "__main__":
    main()
