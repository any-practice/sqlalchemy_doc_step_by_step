from uuid import uuid4


def test1():
    from sqlalchemy import text

    from src.core.engine import engine

    with engine.connect() as connection:
        result = connection.execute(text("select 'hello world'"))
        print(result.all())


def test2():
    from sqlalchemy import text

    from src.core.engine import engine

    with engine.connect() as connection:
        connection.execute(text("CREATE TABLE some_table (x int, y int)"))
        connection.execute(
            text("INSERT INTO some_table (x, y) VALUES (:x, :y)"),
            [
                {
                    'x': 1,
                    'y': 2,
                },
                {
                    'x': 3,
                    'y': 4,
                },
            ],
        )
        connection.commit()


def test3():
    from sqlalchemy import text

    from src.core.engine import engine

    with engine.connect() as connection:
        result = connection.execute(text('SELECT x, y FROM some_table'))
        for row in result:
            print(f'x = {row.x}, y = {row.y}')


def test4():
    from sqlalchemy.orm import Session
    from sqlalchemy import text

    from src.core.engine import engine

    stmt = 'SELECT x, y FROM some_table'
    with Session(engine) as session:
        result = session.execute(text(stmt))
        for row in result:
            print(f'x = {row.x}, y = {row.y}')


def test5():
    from sqlalchemy.orm import Session
    from sqlalchemy import text

    from src.core.engine import engine

    stmt = '''
        UPDATE some_table SET x = :x
        WHERE y = :y
    '''
    with Session(engine) as session:
        session.execute(text(stmt), params={'x': 10, 'y': 4})
        session.commit()


def test6():
    from src.core.tables import user_table

    print(user_table.c.name)
    print(user_table.c.keys())    # ['id', 'name', 'fullname']
    print(user_table.primary_key)


def test7():
    # без этого импорта metadata_obj не узнает о существование этих таблиц
    from src.core.tables import user_table, address_table    # noqa
    from src.core.engine import engine
    from src.core.metadata_obj import meta_data_obj

    meta_data_obj.create_all(engine)


def test8():
    from sqlalchemy import text
    from sqlalchemy.orm import Session

    from src.core.engine import engine

    stmt = 'DROP TABLE user_table, address_table'
    with Session(engine) as session:
        session.execute(text(stmt))
        session.commit()


def test9():
    from src.orm.declarative_models.case1.base import Base
    from src.core.engine import engine

    # без этого импорта metadata_obj не узнает о существование этих таблиц
    from src.orm.declarative_models.case1 import User, Address    # noqa

    Base.metadata.create_all(engine)


def test10():
    from sqlalchemy.orm import Session

    from src.core.engine import engine
    from src.orm.declarative_models.case1 import User, Address

    with Session(engine) as session:
        alex = User(
            name='alex',
            fullname='alex sosov',
            addresses=[
                Address(email_address='alex_sosov@yandex.ru'),
                Address(email_address='alex_sosov@mirea.ru'),
            ],
        )

        session.add(alex)
        session.commit()


def test11():
    from sqlalchemy import select
    from sqlalchemy.orm import Session

    from src.core.engine import engine
    from src.orm.declarative_models.case1 import User, Address

    with Session(engine) as session:
        # использую join, чтобы сделать фильтра по полю сджоенной таблицы
        stmt = select(Address).join(User).where(User.name == 'alex')
        user_adress = session.scalar(stmt)

        print(user_adress.email_address)


def test12():
    from sqlalchemy import select
    from sqlalchemy.orm import Session

    from src.core.engine import engine
    from src.orm.declarative_models.case1 import User, Address

    with Session(engine) as session:
        # использую join, чтобы сделать фильтра по полю сджоенной таблицы
        stmt = select(Address).where(User.name == 'alex')
        user_adress = session.scalar(stmt)

        print(user_adress.email_address)


def test13():

    from sqlalchemy import select
    from sqlalchemy.orm import Session

    from src.core.engine import engine
    from src.orm.declarative_models.case1 import User, Address

    with Session(engine) as session:
        user = session.scalar(select(User))
        new_address = Address(email_address='alexey_sosov@zyfra.com')

        user.addresses.append(new_address)
        # мы создали address и положи его в список адрессов пользователя
        # но у new_address также проставился пользователь
        print(new_address.user)

        session.commit()

        # после того как мы вызвали session.commit()
        # вызывается Session.commit.expire_on_commit()
        # и теперь, если мы обратимся к user.id, то опять пойдет запрос в бд
        print(user.id)

        # также, если обратимся к user.addresses, то мы получим уже персистентый список


def test14():
    """решение проблемы N + 1"""

    from sqlalchemy import select
    from sqlalchemy.orm import Session, selectinload

    from src.core.engine import engine
    from src.orm.declarative_models.case1 import User

    with Session(engine) as session:

        user = session.scalar(select(User).options(selectinload(User.addresses)))

        print(user.addresses)


def test15():
    """select related"""

    from sqlalchemy import select
    from sqlalchemy.orm import Session, joinedload

    from src.core.engine import engine
    from src.orm.declarative_models.case1 import Address

    with Session(engine) as session:

        address = session.scalar(select(Address).options(joinedload(Address.user)))
        print(address.user.fullname)


def test16():
    """
    load concrete fields
    """
    from sqlalchemy import select
    from sqlalchemy.orm import Session, load_only

    from src.core.engine import engine
    from src.orm.declarative_models.case1 import User

    with Session(engine) as session:
        # user = session.scalar(select(User.fullname).where(User.id == 1))
        # выше аналогично варианту ниже
        session.scalar(select(User).where(User.id == 1).options(load_only(User.fullname)))


def test17():
    """
    update without load to python memory
    """

    from sqlalchemy import update
    from sqlalchemy.orm import Session

    from src.core.engine import engine
    from src.orm.declarative_models.case1 import User

    with Session(engine) as session:
        # yapf: disable
        stmt = (
            update(User).
            where(User.id == 1).
            values(fullname=str(uuid4()))
        )
        # yapf: enable
        session.execute(stmt)
        session.commit()


def test18():
    """
    insert with returning
    """

    from sqlalchemy import insert
    from sqlalchemy.orm import Session

    from src.core.engine import engine
    from src.orm.declarative_models.case1 import User

    with Session(engine) as session:
        # yapf:disable
        stmt = (
            insert(User).
            values(name='lera', fullname='lera rybalchenkova').
            returning(User.id)
        )
        # yapf:enable
        id_of_created_user = session.scalar(stmt)
        session.commit()
        print(id_of_created_user)    # тут будет какое то значение


# main = test1
# main = test2
# main = test3
# main = test4
# main = test5
# main = test6
# main = test7
# main = test8
# main = test9
# main = test10
# main = test11
# main = test12
# main = test13
# main = test14
# main = test15
# main = test16
# main = test17
main = test18

if __name__ == '__main__':
    main()
