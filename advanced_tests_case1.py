def test1():
    """
    что такое expire_all
    """

    from sqlalchemy import select
    from sqlalchemy.orm import Session

    from src.core.engine import engine
    from src.orm.declarative_models import User, Address

    with Session(engine) as session:
        # использую join, чтобы сделать фильтра по полю сджоенной таблицы
        stmt = select(Address).join(User).where(User.name == 'alex')
        user_adress = session.scalar(stmt)

        session.expire_all()

        # т.к. я ручками вызвал expire_all, то теперь при обращении к email_address снова пойдут запрос в бд
        print(user_adress.email_address)

        # expire_all автоматом вызывается после session.commit


def test2():
    """
    хочу залезть в session.identity_map
    """
    from sqlalchemy import select
    from sqlalchemy.orm import Session

    from src.core.engine import engine
    from src.orm.declarative_models import User, Address

    with Session(engine) as session:
        # использую join, чтобы сделать фильтра по полю сджоенной таблицы
        stmt = select(Address).join(User).where(User.name == 'alex')
        session.scalar(stmt)

        for value in session.identity_map.values():
            print(value)    # тут будет экземпляр адресса


def test3():
    """
    проверю, создаем ли сессия транзакцию автоматически
    """

    from sqlalchemy.orm import Session

    from src.core.engine import engine

    with Session(engine) as session:
        print(session.in_transaction())    # False


def test4():
    """
    в кейсе 3 я убедился, что Session.__enter__ не создает транзакцию автоматически
    вопрос как ее создать тогда

    Ответ: в sqlalchemy используется "virtual" транзакция SessionTransaction
    """


def test5():
    """
    session.is_modified | session.dirty
    """

    from sqlalchemy import select
    from sqlalchemy.orm import Session

    from src.core.engine import engine
    from src.orm.declarative_models import User, Address

    with Session(engine) as session:
        stmt = select(Address).where(User.name == 'alex')
        user_adress = session.scalar(stmt)

        user_adress.email_address = 'some_adress'

        for obj in session.dirty:
            print(obj)    # Address

        print(session.is_modified(user_adress))    # True


def test6():
    """
    session.flush
    """

    from sqlalchemy import select
    from sqlalchemy.orm import Session

    from src.core.engine import engine
    from src.orm.declarative_models import User, Address

    with Session(engine) as session:
        stmt = select(Address).where(User.name == 'alex')
        user_adress = session.scalar(stmt)

        user_adress.email_address = 'some_adress'

        session.flush((user_adress, ))
        # через session.flush мы переводим изменения из python в database контекст
        # если посмотреть на запросы, то появляется
        # UPDATE address_table SET email_address=%(email_address)s WHERE address_table.id = %(address_table_id)s
        #
        # без flush этого запроса не будет

        print(user_adress.email_address)    # 'some_adress


def test7():
    """
    session.commit

    session.commit делает следующее
    1. session.flush
    2. session.expire_all
    3. коммитит изменения в бд и завершает транзакцию
    """


def test8():
    """
    rollback
    """

    from sqlalchemy import select
    from sqlalchemy.orm import Session

    from src.core.engine import engine
    from src.orm.declarative_models import User, Address

    with Session(engine) as session:
        stmt = select(Address).where(User.name == 'alex')
        user_adress = session.scalar(stmt)

        user_adress.email_address = 'some_address'

        print(user_adress.email_address)    # some_address
        session.rollback()
        print(user_adress.email_address)    # alex_sosov@yandex.ru


def test9():
    """
    хочу попробовать обновить связанную one_to_many запись без selectinload, а затем
    обратиться к списку связанных записей и посмотреть есть ли там эти изменения

    По итогу если мы изменим адресс и потом достанем из бд связанные адресса с пользователем,
    то там точно также увидим обновленный адресс
    """

    from sqlalchemy import select
    from sqlalchemy.orm import Session, selectinload

    from src.core.engine import engine
    from src.orm.declarative_models import User, Address

    with Session(engine) as session:
        address = session.scalar(select(Address).where(Address.id == 1))
        address.email_address += ' tmp'

        session.flush()

        stmt = select(User).options(selectinload(User.addresses))
        user = session.scalar(stmt)

        for address in user.addresses:
            if address.id == 1:
                print(address.email_address)    # ... + tmp # т.е. измененное значение


def test10():
    """
    selectinload конкретные записи
    """

    from sqlalchemy import select
    from sqlalchemy.orm import Session, selectinload

    from src.core.engine import engine
    from src.orm.declarative_models import User, Address

    with Session(engine) as session:
        # yapf: disable
        stmt = select(
            User,
        ).where(
            User.id == 1,
        ).options(
            selectinload(User.addresses.and_(Address.email_address != 'alex_sosov@mirea.ru')),
        )
        # yapf: enable
        user = session.scalar(stmt)

        for address in user.addresses:
            print(address.email_address)


def test11():
    """
    joinedload c перечислением полей
    """
    from sqlalchemy import select
    from sqlalchemy.orm import Session, joinedload, load_only

    from src.core.engine import engine
    from src.orm.declarative_models import User, Address

    with Session(engine) as session:
        # yapf: disable
        stmt = select(
            Address,
        ).join(
            User,
        ).where(
            Address.id == 1,
        ).options(
            load_only(Address.email_address),
            joinedload(
                Address.user,
            ).load_only(
                User.fullname,
            ),
        )
        # yapf: enable

        address = session.scalar(stmt)    # noqa


def test12():
    """
    какое запрос пойдет, если вызвать len(user.address)
    """
    from sqlalchemy import select
    from sqlalchemy.orm import Session

    from src.core.engine import engine
    from src.orm.declarative_models.case1 import User

    with Session(engine) as session:
        # 1 вариант, через len
        stmt = select(User).where(User.id == 1)
        user = session.scalar(stmt)

        # тут пойдет запрос на получение всех адрессов, чего нам не нужно, нам нужно только их количество
        print(len(user.addresses))


def test13():
    """
    joindedload для one to many

    по итогу, связанные one to many адресса можно загрузить в память один запросом вместе с запросом
    на пользователей, а не через 2 запроса как в selectinload
    """

    from sqlalchemy import select
    from sqlalchemy.orm import Session, joinedload

    from src.core.engine import engine
    from src.orm.declarative_models.case1 import User

    with Session(engine) as session:
        # yapf: disable
        stmt = select(
            User,
        ).select_from(
            User,
        ).options(
            joinedload(User.addresses),
        )

        result = session.scalar(stmt)

        for addr in result.addresses:
            print(addr.id)

        print('done')


def test14():
    """
    joinedload vs join

    итог:
    1. joinedload делает join внутри запроса и загоняет данные в питоновскую память
    2. join делает join внутри запроса и загоняет данные в питоновскую память
    """

    from sqlalchemy import select
    from sqlalchemy.orm import Session, joinedload

    from src.core.engine import engine
    from src.orm.declarative_models.case1 import User, Address

    with Session(engine) as session:
        # yapf: disable
        stmt_with_joineload = select(
            User,
        ).select_from(
            User,
        ).options(
            joinedload(User.addresses),
        )
        # yapf: enable
        """
        SELECT
            user_account.id,
            user_account.name,
            user_account.fullname,
            # при этом эти колонки включаются даже когда у нас используется просто select(User,), т.е. без указания Address
            address_table_1.id AS id_1,
            address_table_1.user_id,
            address_table_1.email_address 
        FROM user_account LEFT OUTER JOIN address_table AS address_table_1 ON user_account.id = address_table_1.user_id
        """
        # print(stmt_with_joineload)

        # yapf: disable
        stmt_with_join = select(
            User
        ).select_from(
            User,
        ).join(
            Address,
        )
        # yapf: enable
        """
        SELECT user_account.id, user_account.name, user_account.fullname
        # как мы видим в запросе нет загрузки полей address, только просто join
        FROM user_account JOIN address_table ON user_account.id = address_table.user_id
        """
        print(stmt_with_join)

        # что будет если я включу Address в выборку и захочу на них посмотреть в питоне, будет N + 1?
        # ОТВЕТ: да, будет N + 1

        # yapf: disable
        stmt_with_join_included_address = select(
            User, Address,
        ).select_from(
            User,
        ).join(
            Address,
        )
        # yapf: enable
        """
        SELECT 
            user_account.id,
            user_account.name,
            user_account.fullname,
            address_table.id AS id_1,
            address_table.user_id,
            address_table.email_address 
        FROM user_account JOIN address_table ON user_account.id = address_table.user_id
        """
        print(stmt_with_join_included_address)

        user: User = session.scalar(stmt_with_join_included_address)
        for address in user.addresses:
            print(address)

        print('done')


def test15():
    """
    хочу получить только идентификаторы связанных с User адрессов
    """
    from sqlalchemy import select
    from sqlalchemy.orm import Session, selectinload

    from src.core.engine import engine
    from src.orm.declarative_models.case1 import User, Address

    with Session(engine) as session:
        # yapf: disable
        stmt = select(
            User,
        ).select_from(
            User,
        ).where(
            User.id == 1,
        ).options(
            selectinload(
                User.addresses,
            ).load_only(
                Address.id,
            ),
        )

        # yapf: enable

        user = session.scalar(stmt)
        """
        1-ый запрос
        SELECT user_account.id, user_account.name, user_account.fullname
            FROM user_account
        WHERE user_account.id = %(id_1)s

        2-ой запрос
        SELECT address_table.user_id AS address_table_user_id, address_table.id AS address_table_id
            FROM address_table
        WHERE address_table.user_id IN (%(primary_keys_1)s)
        """

        for address in user.addresses:
            print(address.id)


# main = test1
# main = test2
# main = test3
# main = test5
# main = test6
# main = test8
# main = test9
# main = test10
# main = test11
# main = test12
# main = test13
# main = test14
main = test15

if __name__ == '__main__':
    main()
