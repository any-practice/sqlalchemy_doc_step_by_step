from sqlalchemy import create_engine

DATABASE_NAME = 'sqlalchemy_step_by_step_from_doc'
DB_URL = f'postgresql://postgres@localhost:5432/{DATABASE_NAME}'

engine = create_engine(DB_URL, echo=True)
