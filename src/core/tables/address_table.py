from sqlalchemy import Column, ForeignKey, Integer, String, Table

from ..metadata_obj import meta_data_obj

address_table = Table(
    'address_table',
    meta_data_obj,
    Column('id', Integer, primary_key=True),
    Column('user_id', ForeignKey('user_table.id'), nullable=False),
    Column('email_address', String, nullable=False),
)
