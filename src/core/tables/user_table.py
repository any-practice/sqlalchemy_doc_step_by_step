from sqlalchemy import Column, Integer, String, Table

from ..metadata_obj import meta_data_obj

user_table = Table(
    'user_table',
    meta_data_obj,
    Column('id', Integer, primary_key=True),
    Column('name', String(30)),
    Column('fullname', String),
)
