from __future__ import annotations

from typing import List, TYPE_CHECKING

from sqlalchemy.orm import Mapped, mapped_column, relationship

from .base import Base

if TYPE_CHECKING:
    from .programmer import ProgrammerORM


class DepartamentORM(Base):

    __tablename__ = 'departament'

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column()

    programmers: Mapped[List[ProgrammerORM]] = relationship(back_populates='departament')
