from __future__ import annotations

from typing import TYPE_CHECKING

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from .base import Base

if TYPE_CHECKING:
    from .departament import DepartamentORM


class ProgrammerORM(Base):

    __tablename__ = 'programmer'

    id: Mapped[int] = mapped_column(primary_key=True)
    age: Mapped[int] = mapped_column()
    salary: Mapped[float] = mapped_column()
    departament_id: Mapped[int] = mapped_column(ForeignKey('departament.id'))

    departament: Mapped[DepartamentORM] = relationship(back_populates='programmers')
