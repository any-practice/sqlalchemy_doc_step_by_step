from __future__ import annotations

from typing import List, TYPE_CHECKING

from sqlalchemy import String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from .base import Base

if TYPE_CHECKING:
    from .address import Address


class User(Base):

    __tablename__ = 'user_account'

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(String(30))
    fullname: Mapped[str] = mapped_column(String)

    addresses: Mapped[List[Address]] = relationship(back_populates='user')

    def __repr__(self) -> str:
        return f'<User: id={self.id}, name={self.name}, fullname={self.fullname}>'
