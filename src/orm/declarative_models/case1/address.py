from sqlalchemy import ForeignKey, String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from .base import Base
from .user import User

# mapped_column - это орм версия Column


class Address(Base):

    __tablename__ = 'address_table'

    id: Mapped[int] = mapped_column(primary_key=True)
    user_id: Mapped[int] = mapped_column(ForeignKey('user_account.id'))
    email_address: Mapped[str] = mapped_column(String(30), nullable=False)

    user: Mapped[User] = relationship(back_populates='addresses')

    def __repr__(self) -> str:
        return f'<Address: id={self.id}, user_id={self.user_id}, email_address={self.email_address}>'
